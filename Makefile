.PHONY: default
default: book.pdf

%.pdf: %.md
	pandoc --toc --number-sections -Vsecnumdepth=4 -o $@ $<
