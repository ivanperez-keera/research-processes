---
number-sections: true
toc: true
include-before:
- '`\newpage{}`{=pdf}'
---
\pagebreak

# Processes

## Synchronize dash

- Clear orphan files I need
- Clear orphan repos I need
- Record and save software list
- Record and save config changes
- Git commit and push
- Rsync location
- Rsync home

## Copilot planning for next release

- Review *roadmap*, review *undocumented issues*, file appropriate issues.
- Review *roadmap*, *issues*.
- Shortlist, draft *issues*.
- Estimate effort required, *resources available* in time for next release.
- Decide on *issues*.
- Propose, get approval.
- Plan issues, delegate them.

**Inputs**: Roadmap + Open Issues + Undocumented issues + Resources available

**Outputs**: List of issues + time constraints + expected resources

## Cleaning: Make my bed
- Remove cover, pillows.
- Straighten bottom sheet.
- Put sheet, if applicable.
- Fold sheet, if applicable.
- Fit sheet, if applicable.
- Add pajamas.
- Add pillows.
- Add cover.
- Fold top.
- Add cushions.

## Clean house
- Sort rooms (in order).
- Dust rooms (in order).
- Sweep rooms (in order).
- Clean kitchen.
- Clean bathroom.

## Self care
### Ducha
- Cabelo
- Pel
- Embigo
- Pes pedra pomez

### Baño
- Oídos
- Embigo
- Pelos nariz
- Uñas man cortar
- Uñas pes cortar
- Uñas pes limpar
- Afeitar
- Dentes fío
- Pel cara
- Cremas ferida, pes, pernas
- Talco
